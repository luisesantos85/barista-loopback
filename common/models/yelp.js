'use strict';
var yelpLib = require('yelp');

module.exports = function(Yelp) {
  Yelp.locations = function(cb) {

    console.log('locations service');
    //console.log(cb);
    //console.log(Yelp);


    var response = "";

    var y = new yelpLib({
      consumer_key: 'consumer-key',
      consumer_secret: 'consumer-secret',
      token: 'token',
      token_secret: 'token-secret',
    });
    // See http://www.yelp.com/developers/documentation/v2/search_api

    console.log(y);
    y.search({ term: 'food', location: 'Montreal' })
      .then(function (data) {
        console.log(data);
      })
      .catch(function (err) {
        console.error(err);
      });


    cb(null, response);


  };
  Yelp.remoteMethod(
    'locations', {
      http: {
        path: '/locations',
        verb: 'get'
      },
      returns: {
        arg: 'status',
        type: 'string'
      }
    }
  );
};
