var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.postgres;

var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role',
  'recipe', 'comment', 'entry', 'blog', 'brew'];
ds.autoupdate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' + lbTables + '] updated in ', ds.adapter.name);
  ds.disconnect();
});

