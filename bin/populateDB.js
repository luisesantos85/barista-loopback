/**
 * This script should populate the database with data
 * You can call app.models.{model}.create with a json object with data when
 * creating the data base but there may be a way to populate the already
 * created data base
 * */

/*var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.postgres;

ds.automigrate('recipe', function(err) {
  if (err) throw err;

  var recipes = [
    {
      name: 'Iced Mocca',
      description: 'test',
      createdAt: new Date(),
      lastModifiedAt: new Date()
    },
    {
      name: 'Coconut Latte',
      description: 'test description',
      createdAt: new Date(),
      lastModifiedAt: new Date()
    },
  ];

  var count = recipes.length;
  recipes.forEach(function(recipe) {
    app.models.recipe.create(recipe, function(err, model) {
      if (err) throw err;

      console.log('Created:', model);

      count--;
      if (count === 0)
        ds.disconnect();
    });
  });
});
*/
